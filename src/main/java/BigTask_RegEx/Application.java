package BigTask_RegEx;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * @author Deyneka Oleksandr
 * @version 1.0 01 Dec 2018
 * Class represents 16 tasks from BigTask
 */
public class Application {
    private Map<Integer, String> menu = new LinkedHashMap<>();
    private Map<Integer, Printable> methods = new LinkedHashMap<>();
    private Text text = new Text(
            "C:\\Users\\alex.dey\\IdeaProjects\\task07_String\\text.txt");

    private Application() throws IOException {
    }

    public static void main(String[] args) throws IOException {
        Application consoleMenu = new Application();
        consoleMenu.initMaps();
        while (true) {
            System.out.println("Please, make your choice: ");
            consoleMenu.printMenu();
            consoleMenu.readInput();
        }
    }

    /**
     * method initializes "menu" and "methods"
     */
    private void initMaps() {
        menu.put(1, "Perform Task1");
        menu.put(2, "Perform Task2");
        menu.put(3, "Perform Task3");
        menu.put(4, "Perform Task4");
        menu.put(5, "Perform Task5");
        menu.put(6, "Perform Task6");
        menu.put(7, "Perform Task7");
        menu.put(8, "Perform Task8");
        menu.put(9, "Perform Task9");
        menu.put(10, "Perform Task10");
        menu.put(11, "Perform Task11");
        menu.put(12, "Perform Task12");
        menu.put(13, "Perform Task13");
        menu.put(14, "Perform Task14");
        menu.put(15, "Perform Task15");
        menu.put(16, "Perform Task16");
        menu.put(0, "Exit");

        methods.put(1, text::startTask1);
        methods.put(2, text::startTask2);
        methods.put(3, text::startTask3);
        methods.put(4, text::startTask4);
        methods.put(5, text::startTask5);
        methods.put(6, text::startTask6);
        methods.put(7, text::startTask7);
        methods.put(8, text::startTask8);
        methods.put(9, text::startTask9);
        methods.put(10, text::startTask10);
        methods.put(11, text::startTask11);
        methods.put(12, text::startTask12);
        methods.put(13, text::startTask13);
        methods.put(14, text::startTask14);
        methods.put(15, text::startTask15);
        methods.put(16, text::startTask16);
        methods.put(0, this::startOption0);
    }

    /**
     * method prints out the main menu
     */
    private void printMenu() {
        for (Map.Entry<Integer, String> map : menu.entrySet()) {
            System.out.println(map.getKey() + " " + map.getValue());
        }
    }

    /**
     * method deals with user's input
     */
    private void readInput() {
        Scanner scanner = new Scanner(System.in);
        int res = scanner.nextInt();
        while (res < 0 || res > menu.size() - 1) {
            System.out.println("You entered the wrong number. Please, try again...");
            res = scanner.nextInt();
        }
        methods.get(res).print();
    }

    /**
     * method performs some action
     */
    private void startOption1() {
        System.out.println("Option1 started");
    }

    /**
     * method performs some action
     */
    private void startOption2() {
        System.out.println("Option2 started");
    }

    /**
     * method performs some action
     */
    private void startOption3() {
        System.out.println("Option3 started");
    }

    /**
     * method performs some action
     */
    private void startOption4() {
        System.out.println("Option4 started");
    }

    /**
     * method ends the program
     */
    private void startOption0() {
        System.exit(0);
    }
}
