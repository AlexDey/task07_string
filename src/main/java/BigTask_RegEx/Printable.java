package BigTask_RegEx;

/**
 * functional interface
 */
public interface Printable {
    void print();
}
