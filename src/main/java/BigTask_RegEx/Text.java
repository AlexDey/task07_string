package BigTask_RegEx;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Text {
    private String fileName;
    private static StringBuilder sb = new StringBuilder();
    private static List<String> sentencesList;
    private static List<String> sentencesWithoutPunctuation;
    private static List<String> words;

    Text(String fileName) throws IOException {
        this.fileName = fileName;
        start();
    }

    private void start() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        while (reader.ready()) {
            sb.append(reader.readLine()
                    .replaceAll("\\s+", " ")
                    .trim()).append(" ");
        }
        initLists();
    }

    private void initLists() {
        sentencesList = new ArrayList<>();
        Pattern p = Pattern.compile("([^.!?]|\\.[\\w\\d])+[!?.](\\s|\\))");
        Matcher m = p.matcher(Text.sb);
        while (m.find()) {
            sentencesList.add(Text.sb
                    .substring(m.start(), m.end())
                    .replaceAll("[^\\w\\s.?!]", "")
                    .trim());
        }

        sentencesWithoutPunctuation = new ArrayList<>();
        sentencesList.forEach(n -> sentencesWithoutPunctuation
                .add(" " + n.replaceAll("[.?!]", "")
                        .toLowerCase() + " "));

        words = sentencesWithoutPunctuation.stream()
                .flatMap(e -> Stream.of(e.toLowerCase().split(" ")))
                .filter(string -> !string.isEmpty())
                .collect(Collectors.toList());
    }

    /*
    1.	Знайти найбільшу кількість речень тексту, в яких є однакові слова.
     */
    void startTask1() {
        List<String> temp = words.stream()
                .distinct()
                .collect(Collectors.toList());
        int max = 0;
        int count = 0;
        for (String s : temp) {
            Pattern p = Pattern.compile("[ ]" + s + "[ ]");
            for (String s2 : sentencesWithoutPunctuation) {
                Matcher m = p.matcher(s2);
                if (m.find()) {
                    count++;
                }
            }
            if (max < count) {
                max = count;
            }
            count = 0;
        }
        System.out.println(max);
    }

    /*
    2.	Вивести всі речення заданого тексту у порядку зростання
    кількості слів у кожному з них
     */
    void startTask2() {
        Map<String, Integer> temp = new TreeMap<>();
        for (String s : sentencesWithoutPunctuation) {
            int wordsNumber = s.trim().split(" ").length;
            temp.put(s, wordsNumber);
        }
        temp = sortByValues(temp, true);
        System.out.println("All sentences in ascending order by words count: ");
        temp.forEach((n, l) -> System.out.println(n.trim()));
    }

    /*
    3.	Знайти таке слово у першому реченні, якого немає ні в одному з інших речень
     */
    void startTask3() {
        String[] temp = sentencesWithoutPunctuation.get(0).trim().split(" ");
        boolean indicator = false;
        for (String s : temp) {
            if (indicator) {
                break;
            }
            for (int i = 1; i < sentencesWithoutPunctuation.size(); i++) {
                if (sentencesWithoutPunctuation.get(i).contains(s)) {
                    break;
                } else {
                    System.out.println("Non-repeated word in first sentence is: " + s);
                    indicator = true;
                    break;
                }
            }
        }
        if (!indicator) {
            System.out.println("Non-repeated word in first sentence is: every word is repeated");
        }
    }

    /*
    4.	У всіх запитальних реченнях тексту знайти і надрукувати без повторів
    слова заданої довжини.
     */
    void startTask4() {
        int length = 5;
        List<String> temp = sentencesList.stream()
                .filter(n -> Pattern.compile(".+\\?").matcher(n).matches())
                .distinct().collect(Collectors.toList());
        System.out.print("Words of length " + length + " from question sentences are: ");
        List<String> temp2 = temp.stream().flatMap(n -> Stream.of(n.split(" ")))
                .filter(n -> n.replace("?", "").length() == length)
                .collect(Collectors.toList());
        if (temp2.isEmpty()) {
            System.out.println("no words with length " + length);
        } else {
            temp2.forEach(n -> System.out.print(n
                    .replace("?", "")
                    .toLowerCase() + " "));
        }
        System.out.println();
    }

    /*
    5.	У кожному реченні тексту поміняти місцями перше слово,
    що починається на голосну букву з найдовшим словом.
     */
    void startTask5() {
        for (String s : sentencesWithoutPunctuation) {
            List<String> list = Arrays.stream(s.split(" "))
                    .map(String::trim)
                    .collect(Collectors.toList());
            String vowel = list.stream()
                    .filter(n -> Pattern.compile("[euioa]\\w+").matcher(n).matches())
                    .findFirst()
                    .orElse("null");
            if (!vowel.equals("null")) {
                String max = list.stream().max(Comparator.comparingInt(String::length)).get();
                for (int i = 0; i < list.size(); i++) {
                    int index = list.indexOf(max);
                    if (list.get(i).equals(vowel)) {
                        list.set(i, max);
                        list.set(index, vowel);
                        break;
                    }
                }
                list.forEach(n -> System.out.print(n + " "));
                System.out.println();
            }
        }
    }

    /*
    6.	Надрукувати слова тексту в алфавітному порядку по першій букві.
    Слова, що починаються з нової букви, друкувати з абзацного відступу.
     */
    void startTask6() {
        Map<String, List<String>> map = words.stream()
                .collect(Collectors.groupingBy(n -> n.substring(0, 1)));
        Map<String, List<String>> map2 = new TreeMap<>(map);
        map2.forEach((n, l) -> System.out.println(n + " " + l));
    }

    /*
    6.	Надрукувати слова тексту в алфавітному порядку по першій букві.
    Слова, що починаються з нової букви, друкувати з абзацного відступу.
     */
    void startTask7() {
        System.out.println(words.stream()
                .sorted(Comparator.comparingDouble(this::findPersentage))
                .collect(Collectors.toList()));
    }

    private double findPersentage(String s) {
        int countVowels = 0;
        Matcher matcher = Pattern.compile("[euioa]").matcher(s);
        while (matcher.find()) {
            countVowels++;
        }
        return 100.0 * countVowels / s.length();
    }

    /*
    8.	Слова тексту, що починаються з голосних букв,
    відсортувати в алфавітному порядку по першій приголосній букві слова.
     */
    void startTask8() {
        Map<String, List<String>> map = words.stream()
                .filter(n -> Pattern.compile("[euioa]\\w+").matcher(n).matches())
                .filter(n -> Pattern.compile("\\w[^euioa]\\w+").matcher(n).matches())
                .sorted()
                .collect(Collectors.groupingBy(n -> n.substring(1, 2)));
        Map<String, List<String>> map2 = new TreeMap<>(map);
        map2.forEach((n, l) -> System.out.println(n + " " + l));
    }

    /*
    9.	Всі слова тексту відсортувати за зростанням кількості заданої букви у слові.
    Слова з однаковою кількістю розмістити у алфавітному порядку.
     */
    void startTask9() {
        String someLetter = "s";
        List<String> list = words.stream()
                .filter(n -> n.contains(someLetter))
                .sorted().collect(Collectors.toList());
        list.forEach(System.out::println);
    }

    /*
    10. Є текст і список слів. Для кожного слова з заданого списку знайти,
    скільки разів воно зустрічається у кожному реченні, і відсортувати слова
    за спаданням загальної кількості входжень
     */
    void startTask10() {
        List<String> someWords = Arrays.asList("there", "will", "you", "are", "in", "a");
        Map<String, Long> tempMap = new TreeMap<>();
        for (String s : someWords) {
            long l = words.stream()
                    .filter(n -> n.equals(s)).count();
            tempMap.put(s, l);
        }
        System.out.println(sortByValues(tempMap, false));

    }

    /*
    11.	У кожному реченні тексту видалити підрядок максимальної довжини,
    що починається і закінчується заданими символами
     */
    void startTask11() {
        for (String s : sentencesWithoutPunctuation) {
            String start = "v";
            String end = "m";
            StringBuilder sb = new StringBuilder(s);
            Pattern pattern = Pattern.compile(start + "[\\w\\s]+" + end);
            Matcher matcher = pattern.matcher(sb);
            if (matcher.find()) {
                sb.delete(matcher.start(), matcher.end());
            }
            System.out.println(sb.toString());
        }
    }

    /*
    12.	З тексту видалити всі слова заданої довжини, що починаються
    на приголосну букву
     */
    void startTask12() {
        System.out.println(words.stream()
                .filter(n -> !Pattern.compile("[^auioe]\\w{6}")
                        .matcher(n.trim()).matches())
                .collect(Collectors.toList()));
    }

    /*
    13.	Відсортувати слова у тексті за спаданням кількості
    входжень заданого символу, а у випадку рівності – за алфавітом
     */
    void startTask13() {
        System.out.println("To be done...");
    }

    /*
    14.	У заданому тексті знайти підрядок максимальної довжини,
    який є паліндромом, тобто, читається зліва на право і
    справа на ліво однаково.
     */
    void startTask14() {
        System.out.println("To be done...");
    }

    /*
    15.	Перетворити кожне слово у тексті, видаливши з нього всі
    наступні (попередні) входження першої (останньої) букви цього слова.
     */
    void startTask15() {
        boolean choice = true;
        words.forEach(s -> {
            String start;
            Matcher m;
            if (choice) {
                start = s.substring(0, 1);
                Pattern p = Pattern.compile(start);
                m = p.matcher(s.substring(1));
                System.out.print(start + m.replaceAll("") + " ");
            } else {
                start = s.substring(s.length() - 1);
                Pattern p = Pattern.compile(start);
                m = p.matcher(s.substring(0, s.length() - 1));
                System.out.print(m.replaceAll("") + start + " ");
            }
        });
    }

    /*
    16.	У певному реченні тексту слова заданої довжини замінити
    вказаним підрядком, довжина якого може не співпадати з довжиною слова.
     */
    void startTask16() {
        int sentenceIndex = 0;
        int wordLength = 4;
        String replacement = "oops!";
        Arrays.stream(sentencesWithoutPunctuation.get(sentenceIndex).split(" "))
                .filter(n -> n.length() != 0)
                .forEach(n -> {
                    StringBuilder sb = new StringBuilder();
                    if (n.length() == wordLength) {
                        sb.append(replacement).append(" ");
                    } else {
                        sb.append(n.trim()).append(" ");
                    }
                    System.out.print(sb);
                });
    }

    /*
    method sorts map by values in ascending order if "order" == true;
    and descending order if not
    */
    private static <K, V extends Comparable<V>> Map<K, V> sortByValues(final Map<K, V> map, boolean order) {
        Comparator<K> valueComparator = (k1, k2) -> {

            int compare = order ? map.get(k1).
                    compareTo(map.get(k2)) : map.get(k2).compareTo(map.get(k1));
            if (compare == 0) return 1;
            else return compare;
        };
        Map<K, V> sortedByValues = new TreeMap<>(valueComparator);
        sortedByValues.putAll(map);
        return sortedByValues;
    }
}
