package Task_StringUtils;

import java.util.Arrays;
import java.util.regex.*;

/**
 * @author Deyneka Oleksandr
 * @version 1.0 01 Dec 2018
 * Class StringUtils with an undefined number of parameters of
 * any class that concatenates all parameters and returns Strings.
 */
public class StringUtils {
    private static final String TESTLINE = "Class StringUtils with an undefined number of " +
            "parameters of any class that concatenates all parameters and returns Strings.";

    public static void main(String[] args) {
        System.out.println(checkSentence());
        Arrays.asList(splitWords()).forEach(n -> System.out.println(n.trim() + " "));
        System.out.println(replaceVowels());
        System.out.println(concatString("hello"));
    }

    /**
     * Concatenates strings
     *
     * @return boolean
     */
    private static String concatString(Object o) {
        StringBuilder sb = new StringBuilder("Some string... ");
        return sb.append(o).append(" ").toString();
    }

    /**
     * Checks a sentence to see that it begins with a capital
     * letter and ends with a period
     *
     * @return boolean
     */
    private static boolean checkSentence() {
        return Pattern.compile("[A-Z][\\w\\d\\s]+\\.")
                .matcher(TESTLINE)
                .matches();
    }

    /**
     * Splits string on the word "of"
     *
     * @return string
     */
    private static String[] splitWords() {
        return TESTLINE.split("of");
    }

    /**
     * Replaces all the vowels in some text with underscores
     *
     * @return string
     */
    private static String replaceVowels() {
        return Pattern.compile("[euioa]")
                .matcher(TESTLINE)
                .replaceAll("_");
    }
}
